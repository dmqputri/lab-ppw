"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from lab_1.views import index as index_lab1
from Lab_4.views import home
from Lab_4.views import education
from Lab_4.views import experience
from Lab_4.views import contact
from Lab_4.views import register
from Lab_4.views import fillact
from Lab_4.views import saveact
from Lab_4.views import showact
from Lab_4.views import deleteact

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^$', home, name='home'),
    re_path(r'^story3', home, name='homeprofile'),
    re_path(r'^education', education, name='education'),
    re_path(r'^experience', experience, name='experience'),
    re_path(r'^contact', contact, name='contact'),
    re_path(r'^register', register, name='register'),
    re_path(r'^Lab_4', home, name='homeey'),
    re_path(r'^fillact',fillact,name='fillact'),
    re_path(r'^saveact', saveact, name='saveact'),
    re_path(r'^showact',showact,name='showact'),
    re_path(r'^deleteact',deleteact,name='deleteact'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
