from django.db import models
from datetime import datetime, date


# Create your models here.
class Aktivitas(models.Model):
    days = models.CharField(max_length =10)
    dates = models.DateField()
    time = models.TimeField()
    activity = models.CharField(max_length = 30)
    place = models.CharField(max_length = 20)
    category = models.CharField(max_length = 20)