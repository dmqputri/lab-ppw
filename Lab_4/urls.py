from django.conf.urls import url
from . import views

urlpatterns = [
 url('', views.home, name = 'home'),
 url('education/', views.education, name = 'education'),
 url('experience/', views.experience, name = 'experience'),
 url('contact/', views.contact, name = 'contact'),
 url('register/', views.register, name = 'register'),
 url('saveact/',views.saveact, name = 'saveact'),
 url('fillact/',views.fillact, name = 'fillact'),
 url('showact/',views.showact, name = 'showact'),
 url('deleteact/',views.deleteact, name = 'deleteact')
]

