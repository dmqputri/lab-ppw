from django.shortcuts import render
from django.http import HttpResponse
from .models import Aktivitas
from .forms import Message_Form

# Create your views here.

mhs_name = 'ptr'
def home(request):
    return render(request,'story3.html')
def education(request):
    return render(request,'education.html')
def experience(request):
    return render(request,'experience.html')
def contact(request):
    return render(request,'contact.html')
def register(request):
    return render(request,'challenge.html')
response = {}
def saveact(request):
    #form = Forms(request.POST or None)
    if(request.method == 'POST'):
        response['days'] = request.POST['days']
        response['dates'] = request.POST['dates']
        response['time'] = request.POST['time']
        response['activity'] = request.POST['activity']
        response['place'] = request.POST['place']
        response['category'] = request.POST['category']
        jadwal = Aktivitas(days = response['days'],dates=response['dates'],time=response['time'],activity=response['activity'],place=response['place'],category=response['category'])
        jadwal.save()
        html = 'form_field5.html'
        return render(request,html,response)
    else:
        return HttpResponse('form_field5.html')
def fillact(request):
    response['saveact'] = Message_Form
    return render(request, 'form_field5.html', response)
def showact(request):
    jadwal = Aktivitas.objects.all()
    response['jadwal'] = jadwal
    return render(request, 'form_result.html',response)
def deleteact(request):
    jadwal = Aktivitas.objects.all().delete()
    response['jadwal'] = jadwal
    return render(request, 'form_result.html',response)

